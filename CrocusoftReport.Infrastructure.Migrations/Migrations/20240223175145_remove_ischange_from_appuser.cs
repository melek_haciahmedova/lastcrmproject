﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CrocusoftReport.Infrastructure.Migrations.Migrations
{
    public partial class remove_ischange_from_appuser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsChanged",
                table: "users");

            migrationBuilder.AlterColumn<string>(
                name: "otp",
                table: "users",
                type: "nvarchar(450)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_users_otp",
                table: "users",
                column: "otp",
                unique: true,
                filter: "[otp] IS NOT NULL");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_users_otp",
                table: "users");

            migrationBuilder.AlterColumn<string>(
                name: "otp",
                table: "users",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)",
                oldNullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsChanged",
                table: "users",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }
    }
}

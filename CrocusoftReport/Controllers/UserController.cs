﻿using CrocusoftReport.CustomAuthorizeLogic;
using CrocusoftReport.Extensions;
using CrocusoftReport.Identity.Queries;
using CrocusoftReport.Identity.ViewModels;
using CrocusoftReport.Infrastructure.Constants;
using CrocusoftReport.SharedKernel.Domain.Seedwork;
using CrocusoftReport.UserDetails.Commands;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CrocusoftReport.Controllers
{
    [Route("user")]
    public class UserController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly IUserQueries _userQueries;
        public UserController(IMediator mediator, IUserQueries userQueries)
        {
            _mediator = mediator;
            _userQueries = userQueries;
        }

        [AuthorizeRoles(CustomRole.SuperAdmin, CustomRole.Admin)]
        [HttpPost("register")]
        public async Task<IActionResult> Register([FromBody] RegisterUserCommand command, [FromHeader(Name = "x-requestid")] string requestId)
        {
            await _mediator.ExecuteIdentifiedCommand<RegisterUserCommand, bool>(command, requestId);
            return Ok(new { Message = "User created successfully!" });
        }

        [Authorize]
        [HttpPut("changePassword")]
        public async Task<IActionResult> ChangePassword([FromBody] ChangePasswordCommand command, [FromHeader(Name = "x-requestid")] string requestId)
        {
            await _mediator.ExecuteIdentifiedCommand<ChangePasswordCommand, bool>(command, requestId);
            return Ok(new { Message = "Password changed successfully!" });
        }

        [AuthorizeRoles(CustomRole.Admin, CustomRole.SuperAdmin)]
        [HttpPatch("resetPassword")]
        public async Task<IActionResult> ResetPassword([FromBody] ResetPasswordCommand command, [FromHeader(Name = "x-requestid")] string requestId)
        {
            await _mediator.ExecuteIdentifiedCommand<ResetPasswordCommand, bool>(command, requestId);
            return Ok(new { Message = "Operation completed successfully!" });
        }

        [AuthorizeRoles(CustomRole.SuperAdmin, CustomRole.Admin)]
        [HttpPut]
        public async Task<IActionResult> Update([FromBody] UpdateUserCommand command, [FromHeader(Name = "x-requestid")] string requestId)
        {
            await _mediator.ExecuteIdentifiedCommand<UpdateUserCommand, bool>(command, requestId);
            return NoContent();
        }

        [HttpPost("email")]
        public async Task<IActionResult> OtpConfirmation([FromBody] SendEmailCommand command, [FromHeader(Name = "x-requestid")] string requestId)
        {
            await _mediator.ExecuteIdentifiedCommand<SendEmailCommand, bool>(command, requestId);
            return Ok(new { Message = "Operation completed successfully!" });
        }

        [AllowAnonymous]
        [HttpPut("passwordConfirmation")]
        public async Task<IActionResult> ForgotPassword([FromBody] PasswordConfirmationCommand command, [FromHeader(Name = "x-requestid")] string requestId)
        {
            await _mediator.ExecuteIdentifiedCommand<PasswordConfirmationCommand, bool>(command, requestId);
            return Ok(new { Message = "Password changed successfully!" });
        }

        [HttpPost("otp")]
        public async Task<IActionResult> OtpConfirmation([FromBody] OtpConfirmationCommand command, [FromHeader(Name = "x-requestid")] string requestId)
        {
            await _mediator.ExecuteIdentifiedCommand<OtpConfirmationCommand, bool>(command, requestId);
            return Ok(new { Message = "Otp sent successfully!" });
        }

        //[HttpPost("validate")]
        //public async Task<IActionResult> ValidateToken([FromBody] CheckTokenVerifyInputCommand command, [FromHeader(Name = "x-requestid")] string requestId)
        //{
        //    await _mediator.ExecuteIdentifiedCommand<CheckTokenVerifyInputCommand, bool>(command, requestId);
        //    return Ok();
        //}

        [AuthorizeRoles(CustomRole.SuperAdmin, CustomRole.Admin)]
        [HttpDelete]
        public async Task<IActionResult> Delete([FromBody] DeleteUserCommand command, [FromHeader(Name = "x-requestid")] string requestId)
        {
            await _mediator.ExecuteIdentifiedCommand<DeleteUserCommand, bool>(command, requestId);
            return NoContent();
        }

        [AuthorizeRoles(CustomRole.SuperAdmin, CustomRole.Admin)]
        [HttpPatch("active")]
        public async Task<IActionResult> Active([FromBody] ActiveUserCommand command, [FromHeader(Name = "x-requestid")] string requestId)
        {
            await _mediator.ExecuteIdentifiedCommand<ActiveUserCommand, bool>(command, requestId);
            return NoContent();
        }

        [AuthorizeRoles(CustomRole.SuperAdmin, CustomRole.Admin, CustomRole.Head)]
        [HttpGet]
        public async Task<AllUserDto> GetAllAsync(UserRequestDto request, LoadMoreDto dto)

        {
            return await _userQueries.GetAllUsersAsync(request, dto);
        }

        [AuthorizeRoles(CustomRole.SuperAdmin, CustomRole.Admin, CustomRole.Head, CustomRole.Employee)]
        [HttpGet("{id}")]
        public async Task<UserGetByIdDto> GetByIdAsync(int id)
        {
            return await _userQueries.GetUserById(id);
        }
    }
}
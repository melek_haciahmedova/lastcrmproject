using CrocusoftReport.Extensions;
using CrocusoftReport.Infrastructure;
using CrocusoftReport.Infrastructure.Database;
using CrocusoftReport.Infrastructure.DBSeed;
using Microsoft.AspNetCore;
using Microsoft.Extensions.Options;
using Serilog;

namespace CrocusoftReport
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = CreateHostBuilder(args).Build();
            Log.Logger = new LoggerConfiguration()
            .WriteTo.File("logs/log.txt", rollingInterval: RollingInterval.Day)
            .WriteTo.Seq("http://localhost:5341")
            .CreateLogger();

            host.MigrateDbContext<CrocusoftReportDbContext>(async (context, services) =>
            {
                var env = services.GetService<IWebHostEnvironment>();
                var settings = services.GetService<IOptions<CrocusoftReportSettings>>();
                var logger = services.GetService<ILogger<CrocusoftReportDbContextSeed>>();
                var seeder = new CrocusoftReportDbContextSeed();
                await seeder.SeedAsync(context, env, settings, logger);
            });
            host.Run();
        }
        public static IWebHostBuilder CreateHostBuilder(string[] args) =>
        WebHost.CreateDefaultBuilder(args).ConfigureServices((hostContext, services) =>
        {

        }).UseSerilog().UseStartup<Startup>();
    }
}
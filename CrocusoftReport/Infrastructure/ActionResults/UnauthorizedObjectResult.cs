﻿using Microsoft.AspNetCore.Mvc;

namespace CrocusoftReport.Infrastructure.ActionResults
{
    public class UnauthorizedObjectResult : ObjectResult
    {
        public UnauthorizedObjectResult(object error) : base(error)
        {
            StatusCode = StatusCodes.Status401Unauthorized;
        }
    }
}
﻿using AutoMapper;
using CrocusoftReport.Infrastructure.ErrorHandling;
using System.ComponentModel.DataAnnotations;

namespace CrocusoftReport.Infrastructure.ErrorHandling
{
    public class ExceptionProfile : Profile
    {
        public ExceptionProfile()
        {
            CreateMap<ValidationException, DeveloperException>();
            CreateMap<Exception, DeveloperException>();
        }
    }
}
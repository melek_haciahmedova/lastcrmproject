﻿using FluentValidation.Results;

namespace CrocusoftReport.Infrastructure.ErrorHandling
{
    public class DeveloperException
    {
        public string Message { get; set; }
        public DeveloperException InnerException { get; set; }
        public IEnumerable<ValidationFailure> Errors { get; set; }
    }
}
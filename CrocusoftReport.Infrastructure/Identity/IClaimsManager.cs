﻿using CrocusoftReport.Domain.AggregatesModel.AppUserAggregate;
using System.Security.Claims;

namespace CrocusoftReport.Infrastructure.Identity
{
    public interface IClaimsManager
    {
        int GetCurrentUserId();
        string GetCurrentUserName();
        IEnumerable<Claim> GetUserClaims(AppUser user);
        Claim GetUserClaim(string claimType);
    }
}
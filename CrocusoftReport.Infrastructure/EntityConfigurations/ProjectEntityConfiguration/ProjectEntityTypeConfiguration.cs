﻿using CrocusoftReport.Domain.AggregatesModel.ProjectAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CrocusoftReport.Infrastructure.EntityConfigurations.ProjectEntityConfiguration
{
    public class ProjectEntityTypeConfiguration : IEntityTypeConfiguration<Project>
    {
        public void Configure(EntityTypeBuilder<Project> projectConfiguration)
        {
            projectConfiguration.ToTable("projects");
            projectConfiguration.HasKey(p => p.Id);
            projectConfiguration.Property(p => p.ProjectName).IsRequired().HasMaxLength(50);
        }
    }
}
﻿using CrocusoftReport.Domain.AggregatesModel.AppUserAggregate;
using CrocusoftReport.Domain.AggregatesModel.ProjectAggregate;
using CrocusoftReport.Domain.AggregatesModel.ProjectUserAggregate;
using CrocusoftReport.Domain.AggregatesModel.ReportAggregate;
using CrocusoftReport.Domain.AggregatesModel.RoleAggregate;
using CrocusoftReport.Domain.AggregatesModel.TeamAggregate;
using CrocusoftReport.Infrastructure.EntityConfigurations.AuditEntityConfiguration;
using CrocusoftReport.Infrastructure.EntityConfigurations.IdentityEntityConfiguration;
using CrocusoftReport.Infrastructure.EntityConfigurations.ProjectEntityConfiguration;
using CrocusoftReport.Infrastructure.EntityConfigurations.ProjectUserEntityConfiguration;
using CrocusoftReport.Infrastructure.EntityConfigurations.ReportEntityConfiguration;
using CrocusoftReport.Infrastructure.EntityConfigurations.RoleEntityConfiguration;
using CrocusoftReport.Infrastructure.EntityConfigurations.TeamEntityConfiguration;
using CrocusoftReport.SharedKernel.Domain.Seedwork;
using CrocusoftReport.SharedKernel.Infrastructure;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace CrocusoftReport.Infrastructure.Database
{
    public sealed class CrocusoftReportDbContext : DbContext, IUnitOfWork
    {
        public const string IDENTITY_SCHEMA = "Identity";
        public const string DEFAULT_SCHEMA = "dbo";
        private readonly IMediator _mediator;
        public CrocusoftReportDbContext(DbContextOptions options, IMediator mediator) : base(options)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }
        public DbSet<AppUser> Users { get; private set; }
        public DbSet<Role> Roles { get; private set; }
        public DbSet<Report> Reports { get; private set; }
        public DbSet<Team> Teams { get; private set; }
        public DbSet<Project> Projects { get; private set; }
        public DbSet<ProjectUser> UserProjects { get; private set; }
        public async Task<bool> SaveEntitiesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            await _mediator.DispatchDomainEventsAsync(this);
            await SaveChangesAsync(true, cancellationToken);
            return true;
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new ClientRequestEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new AppUserEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new RoleEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new ReportEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new ProjectEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new TeamEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new ProjectUserEntityTypeConfiguration());
            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.ClientSetNull;
            }
        }
    }
}
﻿namespace CrocusoftReport.Infrastructure.Constants
{
    public class BlobMetadataKeys
    {
        public const string CREATED_BY_ID = "createdbyid";
        public const string CREATED_BY_NAME = "createdbyname";
        public const string LAST_UPDATED_BY_ID = "lastupdatedbyid";
        public const string LAST_UPDATED_BY_NAME = "lastupdatedbyname";
    }
}
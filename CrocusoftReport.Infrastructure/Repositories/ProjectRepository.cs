﻿using CrocusoftReport.Domain.AggregatesModel.ProjectAggregate;
using CrocusoftReport.Infrastructure.Database;
using CrocusoftReport.SharedKernel.Infrastructure;
using Microsoft.EntityFrameworkCore;

namespace CrocusoftReport.Infrastructure.Repositories
{
    public class ProjectRepository : Repository<Project>, IProjectRepository
    {
        public sealed override DbContext Context { get; protected set; }
        public ProjectRepository(CrocusoftReportDbContext context)
        {
            Context = context;
        }
    }
}
﻿using CrocusoftReport.Domain.AggregatesModel.AppUserAggregate;
using CrocusoftReport.Domain.AggregatesModel.UserAggregate;
using CrocusoftReport.Infrastructure.Database;
using CrocusoftReport.SharedKernel.Infrastructure;
using Microsoft.EntityFrameworkCore;

namespace CrocusoftReport.Infrastructure.Repositories
{
    public class AppUserRepository : Repository<AppUser>, IAppUserRepository
    {
        public sealed override DbContext Context { get; protected set; }

        public AppUserRepository(CrocusoftReportDbContext context)
        {
            Context = context;
        }
    }
}
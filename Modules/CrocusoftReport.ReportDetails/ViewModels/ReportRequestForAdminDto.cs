﻿namespace CrocusoftReport.ReportDetails.ViewModels
{
    public class ReportRequestForAdminDto
    {
        public List<int>? ProjectIds { get; set; }
        public List<int>? UserIds { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}
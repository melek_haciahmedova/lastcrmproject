﻿using CrocusoftReport.Infrastructure.Constants;

namespace CrocusoftReport.ReportDetails.ViewModels
{
    public class ReportRequestForUserDto
    {
        public List<int>? ProjectIds { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}
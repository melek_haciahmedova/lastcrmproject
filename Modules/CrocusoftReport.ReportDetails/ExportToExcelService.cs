﻿using ClosedXML.Excel;
using CrocusoftReport.ReportDetails.Queries;
using CrocusoftReport.ReportDetails.ViewModels;

namespace CrocusoftReport.ReportDetails
{
    public class ExportToExcelService
    {
        public static byte[] ExportToExcel(List<ReportDto> reports)
        {
            using var workbook = new XLWorkbook();
            var worksheet = workbook.Worksheets.Add("Reports");

            // Header row
            var headers = new List<string> { "Id", "Project Name", "Note", "User", "Created Date" };
            for (var i = 0; i < headers.Count; i++)
            {
                worksheet.Cell(1, i + 1).Value = headers[i];
            }

            // Data rows
            for (var i = 0; i < reports.Count; i++)
            {
                var report = reports[i];

                NoteService noteService = new();
                noteService.RemoveHtmlTags(report);

                worksheet.Cell(i + 2, 1).Value = report.Id;
                worksheet.Cell(i + 2, 2).Value = report.ProjectName;
                worksheet.Cell(i + 2, 3).Value = report.Note;
                worksheet.Cell(i + 2, 4).Value = report.User?.FullName;
                worksheet.Cell(i + 2, 5).Value = report.CreatedDate;
            }

            // Auto fit columns
            worksheet.Columns().AdjustToContents();

            // Save the workbook to a memory stream
            using var stream = new MemoryStream();
            workbook.SaveAs(stream);
            return stream.ToArray();
        }
    }
}
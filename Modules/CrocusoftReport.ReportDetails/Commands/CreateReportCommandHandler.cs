﻿using CrocusoftReport.Domain.AggregatesModel.ReportAggregate;
using CrocusoftReport.Domain.Exceptions;
using CrocusoftReport.Identity.Auth;
using CrocusoftReport.Infrastructure.Commands;
using CrocusoftReport.Infrastructure.Database;
using CrocusoftReport.Infrastructure.Idempotency;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace CrocusoftReport.ReportDetails.Commands
{
    public class CreateReportCommandHandler : IRequestHandler<CreateReportCommand, bool>
    {
        private readonly IReportRepository _reportRepository;
        private readonly CrocusoftReportDbContext _context;
        private readonly IUserManager _userManager;
        public CreateReportCommandHandler(IReportRepository reportRepository, CrocusoftReportDbContext context, IUserManager userManager)
        {
            _reportRepository = reportRepository;
            _context = context;
            _userManager = userManager;
        }
        public async Task<bool> Handle(CreateReportCommand request, CancellationToken cancellationToken)
        {
            var report = new Report();
            var userId = _userManager.GetCurrentUserId();
            var project = await _context.Projects.Where(x => x.ProjectUsers.Any(x => x.UserId == userId))
                .Include(u => u.ProjectUsers)
                .FirstOrDefaultAsync(p => p.Id == request.ProjectId, cancellationToken: cancellationToken)
                ?? throw new DomainException("The project does not exist");

            if (request.ProjectId == project.Id)
            {
                report.SetDetails(request.Note, project.Id);
                report.SetAuditFields(userId);
            }

            await _reportRepository.AddAsync(report);
            await _reportRepository.UnitOfWork.SaveChangesAsync(cancellationToken);
            return true;
        }
    }
    public class CreateReportIdentifiedCommandHandler : IdentifiedCommandHandler<CreateReportCommand, bool>
    {
        public CreateReportIdentifiedCommandHandler(IMediator mediator, IRequestManager requestManager) : base(mediator, requestManager)
        {
        }
        protected override bool CreateResultForDuplicateRequest()
        {
            return true;
        }
    }
}
﻿using CrocusoftReport.ReportDetails.ViewModels;
using HtmlAgilityPack;

namespace CrocusoftReport.ReportDetails.Queries
{
    public class NoteService
    {
        public  ReportDto RemoveHtmlTags(ReportDto dto)
        {
            HtmlDocument htmlDoc = new HtmlDocument();
            htmlDoc.LoadHtml(dto.Note);
            dto.Note = htmlDoc.DocumentNode.InnerText;
            return dto;
        }
    }
}
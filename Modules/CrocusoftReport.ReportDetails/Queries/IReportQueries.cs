﻿using CrocusoftReport.Infrastructure.Constants;
using CrocusoftReport.ReportDetails.ViewModels;
using CrocusoftReport.SharedKernel.Infrastructure.Queries;

namespace CrocusoftReport.ReportDetails.Queries
{
    public interface IReportQueries : IQuery
    {
        Task<AllReportDto> GetAllAdminAsync(ReportRequestForAdminDto request, LoadMoreDto dto);
        Task<AllReportDto> GetAllUserAsync(ReportRequestForUserDto request, LoadMoreDto dto);
        Task<ReportDto> GetByIdAsync(int id);
    }
}
﻿using CrocusoftReport.Domain.AggregatesModel.UserAggregate;
using CrocusoftReport.Identity.Auth;
using CrocusoftReport.Infrastructure.Database;
using CrocusoftReport.UserDetails.Commands.Models;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Security.Authentication;

namespace CrocusoftReport.UserDetails.Commands
{
    public class RefreshUserTokenCommandHandler : IRequestHandler<RefreshUserTokenCommand, JwtTokenDTO>
    {
        private readonly IUserManager _userManager;
        private readonly IAppUserRepository _userRepository;
        private readonly CrocusoftReportDbContext _context;

        public RefreshUserTokenCommandHandler(IUserManager userManager, IAppUserRepository userRepository, CrocusoftReportDbContext context)
        {
            _userManager = userManager;
            _userRepository = userRepository;
            _context = context;
        }
        public async Task<JwtTokenDTO> Handle(RefreshUserTokenCommand request, CancellationToken cancellationToken)
        {
            var splitToken = request.RefreshToken.Split("_");
            var user = await _context.Users.Include(p => p.Role)
                .FirstOrDefaultAsync(p => p.RefreshToken == request.RefreshToken, cancellationToken: cancellationToken);

            await _userRepository.UnitOfWork.SaveChangesAsync(cancellationToken);

            if (user == null)
                throw new AuthenticationException("Invalid token.");

            if (Convert.ToDateTime(splitToken[2]) < DateTime.Now)
                throw new AuthenticationException("Token is expired.");

            (string token, DateTime expiresAt) = _userManager.GenerateJwtToken(user);

            return new JwtTokenDTO
            {
                Token = token,
                RefreshToken = request.RefreshToken,
                ExpiresAt = expiresAt
            };
        }
    }
}
﻿using CrocusoftReport.Domain.AggregatesModel.UserAggregate;
using CrocusoftReport.Infrastructure.Commands;
using CrocusoftReport.Infrastructure.Idempotency;
using MediatR;

namespace CrocusoftReport.UserDetails.Commands
{
    public class DeleteUserCommandHandler : IRequestHandler<DeleteUserCommand, bool>
    {
        private readonly IAppUserRepository _userRepository;
        public DeleteUserCommandHandler(IAppUserRepository userRepository)
        {
            _userRepository = userRepository;
        }
        public async Task<bool> Handle(DeleteUserCommand request, CancellationToken cancellationToken)
        {
            foreach (var id in request.Id)
            {
                var user = await _userRepository.GetByIdAsync(id);
                user.Delete();
                _userRepository.Update(user);
            }
            await _userRepository.UnitOfWork.SaveChangesAsync(cancellationToken);
            return true;
        }
    }
    public class DeleteUserIdentifiedCommandHandler : IdentifiedCommandHandler<DeleteUserCommand, bool>
    {
        public DeleteUserIdentifiedCommandHandler(IMediator mediator, IRequestManager requestManager) : base(mediator, requestManager)
        {
        }
        protected override bool CreateResultForDuplicateRequest()
        {
            return true;
        }
    }
}
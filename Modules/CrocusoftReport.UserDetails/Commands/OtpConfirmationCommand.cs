﻿using MediatR;

namespace CrocusoftReport.UserDetails.Commands
{
    public class OtpConfirmationCommand : IRequest<bool>
    {
        public string Email { get; set; }
        public string OtpCode { get; set; }
    }
}
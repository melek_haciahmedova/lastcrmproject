﻿using MediatR;

namespace CrocusoftReport.UserDetails.Commands
{
    public class SendEmailCommand : IRequest<bool>
    {
        public string Email { get; set; }
    }
}
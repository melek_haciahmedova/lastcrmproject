﻿using FluentValidation;

namespace CrocusoftReport.UserDetails.Commands
{
    public class RegisterUserCommandValidator : AbstractValidator<RegisterUserCommand>
    {
        public RegisterUserCommandValidator() : base()
        {
            RuleFor(command => command.Password).NotNull().MinimumLength(8)
                .Must(password => password.Any(char.IsUpper))
                .WithMessage("Password must contain at least one uppercase letter.");
            RuleFor(command => command.Email).EmailAddress().NotNull();
            RuleFor(command => command.FirstName).NotNull();
        }
    }
}
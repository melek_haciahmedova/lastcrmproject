﻿using CrocusoftReport.UserDetails.Commands.Models;
using MediatR;

namespace CrocusoftReport.UserDetails.Commands
{
    public class RefreshUserTokenCommand : IRequest<JwtTokenDTO>
    {
        public string RefreshToken { get; set; }
    }
}
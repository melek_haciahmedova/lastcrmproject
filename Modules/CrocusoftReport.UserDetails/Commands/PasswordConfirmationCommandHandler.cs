﻿using CrocusoftReport.Domain.AggregatesModel.UserAggregate;
using CrocusoftReport.Domain.Exceptions;
using CrocusoftReport.Infrastructure.Commands;
using CrocusoftReport.Infrastructure.Idempotency;
using CrocusoftReport.SharedKernel.Infrastructure;
using MediatR;

namespace CrocusoftReport.UserDetails.Commands
{
    public class PasswordConfirmationCommandHandler : IRequestHandler<PasswordConfirmationCommand, bool>
    {
        private readonly IAppUserRepository _userRepository;
        public PasswordConfirmationCommandHandler(IAppUserRepository userRepository)
        {
            _userRepository = userRepository;
        }
        public async Task<bool> Handle(PasswordConfirmationCommand request, CancellationToken cancellationToken)
        {
            var user = _userRepository.Get(x => x.Email == request.Email)
                ?? throw new DomainException("Provided user name is not associated with any account");

            if (user.Otp != null)
            {
                if (request.NewPassword != request.ConfirmNewPassword)
                {
                    throw new DomainException("New password and confirmation password do not match");
                }
                var newPasswordHash = PasswordHasher.HashPassword(request.NewPassword);
                user.ResetPassword(newPasswordHash);
                _userRepository.Update(user);
                await _userRepository.UnitOfWork.SaveChangesAsync(cancellationToken);
                return true;
            }
            else
            {
                throw new DomainException("Error occured while changing password");
            }
        }
    }
    public class PasswordConfirmationIdentifiedCommandHandler : IdentifiedCommandHandler<PasswordConfirmationCommand, bool>
    {
        public PasswordConfirmationIdentifiedCommandHandler(IMediator mediator, IRequestManager requestManager) : base(mediator, requestManager)
        {
        }
        protected override bool CreateResultForDuplicateRequest()
        {
            return true;
        }
    }
}
﻿using CrocusoftReport.Domain.AggregatesModel.UserAggregate;
using CrocusoftReport.Identity.Auth;
using CrocusoftReport.SharedKernel.Infrastructure;
using CrocusoftReport.UserDetails.Commands.Models;
using MediatR;
using System.Security.Authentication;

namespace CrocusoftReport.UserDetails.Commands
{
    public class GetAuthorizationTokenCommandHandler : IRequestHandler<GetAuthorizationTokenCommand, JwtTokenDTO>
    {
        private readonly IUserManager _userManager;
        private readonly IAppUserRepository _userRepository;
        public GetAuthorizationTokenCommandHandler(IUserManager userManager, IAppUserRepository userRepository)
        {
            _userManager = userManager;
            _userRepository = userRepository;
        }
        public async Task<JwtTokenDTO> Handle(GetAuthorizationTokenCommand request, CancellationToken cancellationToken)
        {
            var email = request.Email.ToLower();
            var user = _userRepository.Get(x => x.Email == request.Email, "Role");

            if (user == null || user.PasswordHash != PasswordHasher.HashPassword(request.Password) || user.IsActive == false)
                throw new AuthenticationException("Invalid credentials.");
            (string token, DateTime expiresAt) = _userManager.GenerateJwtToken(user);

            var refreshTokenValue = "";

            if (user.RefreshToken != null)
            {
                var splitToken = user.RefreshToken.Split("_");

                if (Convert.ToDateTime(splitToken[2]) < DateTime.Now)
                {
                    var randomNumber = GenerateRandomNumberExtension.GenerateRandomNumber();
                    var refreshToken = $"{randomNumber}_{user.Id}_{DateTime.UtcNow.AddDays(30)}";
                    refreshTokenValue = refreshToken;
                    user.UpdateRefreshToken(refreshToken);
                }
                else
                {
                    refreshTokenValue = user.RefreshToken;
                }
            }
            else
            {
                var randomNumber = GenerateRandomNumberExtension.GenerateRandomNumber();
                var refreshToken = $"{randomNumber}_{user.Id}_{DateTime.UtcNow.AddDays(30)}";
                refreshTokenValue = refreshToken;
                user.UpdateRefreshToken(refreshToken);
            }
            _userRepository.Update(user);
            await _userRepository.UnitOfWork.SaveChangesAsync(cancellationToken);

            return new JwtTokenDTO
            {
                Token = token,
                RefreshToken = refreshTokenValue,
                ExpiresAt = expiresAt
            };
        }
    }
}
﻿using CrocusoftReport.Domain.AggregatesModel.UserAggregate;
using CrocusoftReport.Domain.Exceptions;
using CrocusoftReport.Infrastructure.Commands;
using CrocusoftReport.Infrastructure.Database;
using CrocusoftReport.Infrastructure.Idempotency;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace CrocusoftReport.UserDetails.Commands
{
    public class OtpConfirmationCommandHandler : IRequestHandler<OtpConfirmationCommand, bool>
    {
        private readonly IAppUserRepository _userRepository;
        private readonly CrocusoftReportDbContext _context;
        public OtpConfirmationCommandHandler(IAppUserRepository userRepository, CrocusoftReportDbContext context)
        {
            _userRepository = userRepository;
            _context = context;
        }
        public async Task<bool> Handle(OtpConfirmationCommand request, CancellationToken cancellationToken)
        {
            var user = await _context.Users.FirstOrDefaultAsync(x => x.Email == request.Email, cancellationToken: cancellationToken)
                 ?? throw new DomainException("Invalid email address");

            if (user.OtpExpiration > DateTime.UtcNow.AddHours(4).AddMinutes(5))
            {
                throw new DomainException("OTP code expired");
            }

            _userRepository.Update(user);
            await _userRepository.UnitOfWork.SaveChangesAsync(cancellationToken);
            return true;
        }
    }
    public class OtpConfirmationIdentifiedCommandHandler : IdentifiedCommandHandler<OtpConfirmationCommand, bool>
    {
        public OtpConfirmationIdentifiedCommandHandler(IMediator mediator, IRequestManager requestManager) : base(mediator, requestManager)
        {
        }
        protected override bool CreateResultForDuplicateRequest()
        {
            return true;
        }
    }
}
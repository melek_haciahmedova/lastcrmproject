﻿using CrocusoftReport.Domain.AggregatesModel.UserAggregate;
using CrocusoftReport.Domain.Exceptions;
using CrocusoftReport.Infrastructure.Commands;
using CrocusoftReport.Infrastructure.Database;
using CrocusoftReport.Infrastructure.Idempotency;
using CrocusoftReport.SharedKernel.Infrastructure.Helper;
using MediatR;

namespace CrocusoftReport.UserDetails.Commands
{
    public class SendEmailCommandHandler : IRequestHandler<SendEmailCommand, bool>
    {
        private readonly CrocusoftReportDbContext _context;
        private readonly IAppUserRepository _userRepository;
        private readonly ISendEmailAsync _sendEmailAsync;
        public SendEmailCommandHandler(IAppUserRepository userRepository, ISendEmailAsync sendEmailAsync, CrocusoftReportDbContext context)
        {
            _userRepository = userRepository;
            _sendEmailAsync = sendEmailAsync;
            _context = context;
        }
        public async Task<bool> Handle(SendEmailCommand request, CancellationToken cancellationToken)
        {
            var user = _userRepository.Get(x => x.Email == request.Email)
                 ?? throw new DomainException("Provided user name is not associated with any account");

            if (string.IsNullOrEmpty(user.Email))
            {
                throw new DomainException("User does not have an email address");
            }

            string otpCode = new Random().Next(100000, 999999).ToString();

            if (_context.Users.Any(x => x.Otp == otpCode))
            {
                otpCode = new Random().Next(100000, 999999).ToString();
            }

            var subject = "Şifrə yeniləmə istəyi";
            string body = @"<html><body><p><strong>Hörmətli, " + user.FirstName + " " + user.LastName + ", </strong> </p><br></br><p>Şifrənizi yeniləmək üçün tələb göndərdiniz. " +
                "Aşağıdakı koddan istifadə edərək şifrənizi yeniləyə bilərsiniz<br></br> <br></br><strong>" + otpCode +
                "<strong></p><br></br>Qeyd: Bu kodu başqa şəxslərlə paylaşmayın" +
                "<p>Hər hansı bir çətinliyiniz olarsa sistem administratoru ilə əlaqə saxlamağınız tövsiyyə olunur.</p>" +
                "</p><br><p> Hörmətlə,</p><p> Crocusoft </p> ";
            user.SetOtp(otpCode);
            _userRepository.Update(user);
            await _userRepository.UnitOfWork.SaveEntitiesAsync(cancellationToken);
            await _sendEmailAsync.SendEmailUserAsync(user.Email, subject, body, null);
            return true;
        }
    }
    public class SendEmailIdentifiedCommandHandler : IdentifiedCommandHandler<SendEmailCommand, bool>
    {
        public SendEmailIdentifiedCommandHandler(IMediator mediator, IRequestManager requestManager) : base(mediator, requestManager)
        {
        }
        protected override bool CreateResultForDuplicateRequest()
        {
            return true;
        }
    }
}
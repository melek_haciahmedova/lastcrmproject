﻿namespace CrocusoftReport.TeamDetails.ViewModels
{
    public class AllTeamDto
    {
        public IEnumerable<TeamDto> Teams { get; set; }
        public int TotalCount { get; set; }
    }
}
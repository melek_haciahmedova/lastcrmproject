﻿using CrocusoftReport.Domain.AggregatesModel.TeamAggregate;
using CrocusoftReport.Domain.Exceptions;
using CrocusoftReport.Infrastructure.Commands;
using CrocusoftReport.Infrastructure.Database;
using CrocusoftReport.Infrastructure.Idempotency;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace CrocusoftReport.TeamDetails.Commands
{
    public class UpdateTeamCommandHandler : IRequestHandler<UpdateTeamCommand, bool>
    {
        private readonly ITeamRepository _teamRepository;
        private readonly CrocusoftReportDbContext _context;
        public UpdateTeamCommandHandler(ITeamRepository teamRepository, CrocusoftReportDbContext context)
        {
            _teamRepository = teamRepository;
            _context = context;
        }
        public async Task<bool> Handle(UpdateTeamCommand request, CancellationToken cancellationToken)
        {
            var team = await _teamRepository.GetByIdAsync(request.Id);
            Team? existingTeam = await _context.Teams
                .FirstOrDefaultAsync(t => t.TeamName.ToLower().Trim() == request.Name.ToLower().Trim() && t.Id != request.Id, cancellationToken: cancellationToken);

            if (existingTeam is not null)
            {
                throw new DomainException($"Team already exists, please choose another name.");
            }

            team.SetDetails(request.Name.Trim());
            _teamRepository.Update(team);
            await _teamRepository.UnitOfWork.SaveChangesAsync(cancellationToken);
            return true;
        }
        public class RegisterTeamIdentifiedCommandHandler : IdentifiedCommandHandler<UpdateTeamCommand, bool>
        {
            public RegisterTeamIdentifiedCommandHandler(IMediator mediator, IRequestManager requestManager) : base(mediator, requestManager)
            {
            }
            protected override bool CreateResultForDuplicateRequest()
            {
                return true;
            }
        }
    }
}
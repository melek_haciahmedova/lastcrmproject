﻿using FluentValidation;

namespace CrocusoftReport.TeamDetails.Commands
{
    public class UpdateTeamCommandValidator : AbstractValidator<UpdateTeamCommand>
    {
        public UpdateTeamCommandValidator() : base()
        {
            RuleFor(t => t.Id).NotEmpty().NotNull();
            RuleFor(t => t.Name).NotEmpty().NotNull();
        }
    }
}
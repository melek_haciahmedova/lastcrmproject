﻿using CrocusoftReport.Domain.AggregatesModel.TeamAggregate;
using CrocusoftReport.Domain.Exceptions;
using CrocusoftReport.Infrastructure.Commands;
using CrocusoftReport.Infrastructure.Database;
using CrocusoftReport.Infrastructure.Idempotency;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace CrocusoftReport.TeamDetails.Commands
{
    public class CreateTeamCommandHandler : IRequestHandler<CreateTeamCommand, bool>
    {
        private readonly ITeamRepository _teamRepository;
        private readonly CrocusoftReportDbContext _context;
        public CreateTeamCommandHandler(ITeamRepository teamRepository, CrocusoftReportDbContext context)
        {
            _teamRepository = teamRepository;
            _context = context;
        }
        public async Task<bool> Handle(CreateTeamCommand request, CancellationToken cancellationToken)
        {
            Team? existingTeam = await _context.Teams
                .FirstOrDefaultAsync(t => t.TeamName.ToLower().Trim() == request.TeamName.ToLower().Trim(), cancellationToken: cancellationToken);

            if (existingTeam is not null)
            {
                throw new DomainException($"Team already exists, please choose another name.");
            }

            var newTeam = new Team();
            newTeam.SetDetails(request.TeamName);
            await _teamRepository.AddAsync(newTeam);
            await _teamRepository.UnitOfWork.SaveEntitiesAsync(cancellationToken);
            return true;
        }
    }
    public class CreateTeamIdentifiedCommandHandler : IdentifiedCommandHandler<CreateTeamCommand, bool>
    {
        public CreateTeamIdentifiedCommandHandler(IMediator mediator, IRequestManager requestManager) : base(mediator, requestManager)
        {
        }
        protected override bool CreateResultForDuplicateRequest()
        {
            return true;
        }
    }
}
﻿using FluentValidation;

namespace CrocusoftReport.TeamDetails.Commands
{
    public class DeleteTeamCommandValidator : AbstractValidator<DeleteTeamCommand>
    {
        public DeleteTeamCommandValidator() : base()
        {
            RuleFor(t => t.Ids).NotNull();
        }
    }
}
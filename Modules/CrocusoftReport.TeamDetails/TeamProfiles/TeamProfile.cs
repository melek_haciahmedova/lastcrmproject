﻿using AutoMapper;
using CrocusoftReport.Domain.AggregatesModel.TeamAggregate;
using CrocusoftReport.Identity.ViewModels;
using CrocusoftReport.TeamDetails.ViewModels;

namespace CrocusoftReport.TeamDetails.TeamProfiles
{
    public class TeamProfile : Profile
    {
        public TeamProfile()
        {
            CreateMap<Team, TeamDto>();
            CreateMap<Team, TeamGetByIdDto>();
            CreateMap<Team, TeamInfoDto>();
        }
    }
}
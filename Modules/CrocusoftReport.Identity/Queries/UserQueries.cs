﻿using AutoMapper;
using CrocusoftReport.Domain.AggregatesModel.AppUserAggregate;
using CrocusoftReport.Domain.Exceptions;
using CrocusoftReport.Identity.Auth;
using CrocusoftReport.Identity.ViewModels;
using CrocusoftReport.Infrastructure.Constants;
using CrocusoftReport.Infrastructure.Database;
using CrocusoftReport.SharedKernel.Domain.Seedwork;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System.Linq.Dynamic.Core;

namespace CrocusoftReport.Identity.Queries
{
    public class UserQueries : IUserQueries
    {
        private readonly CrocusoftReportDbContext _context;
        private readonly IServiceProvider _serviceProvider;
        private readonly IMapper _mapper;
        public UserQueries(CrocusoftReportDbContext context, IMapper mapper, IServiceProvider serviceProvider)
        {
            _context = context;
            _mapper = mapper;
            _serviceProvider = serviceProvider;
        }
        public async Task<AppUser> FindAsync(int userId)
        {
            return await _context.Users.Include(p => p.Role).FirstOrDefaultAsync(p => p.Id == userId);
        }

        public async Task<AppUser> FindByEmailAsync(string email)
        {
            return await _context.Users.FirstOrDefaultAsync(u => u.Email == email);
        }

        public async Task<UserProfileDto> GetUserProfileAsync(int userId)
        {
            var user = await _context.Users.Include(r => r.Role)
                .Where(u => u.Id == userId)
                .AsNoTracking()
                .SingleOrDefaultAsync()
                ?? throw new ArgumentNullException();
            var profile = _mapper.Map<UserProfileDto>(user);
            return profile;
        }

        public async Task<AllUserDto> GetAllUsersAsync(UserRequestDto request, LoadMoreDto dto)
        {
            var userManager = _serviceProvider.GetRequiredService<IUserManager>();
            var currentUser = await userManager.GetCurrentUser();
            var entities = _context.Users.Include(p => p.Role).Include(m => m.Team)
        .Where(p =>
            (string.IsNullOrEmpty(request.FirstName) || p.FirstName.ToLower().Contains(request.FirstName.ToLower())) &&
            (string.IsNullOrEmpty(request.LastName) || p.LastName.ToLower().Contains(request.LastName.ToLower())) && 
            !p.IsDeleted)
       .OrderByDescending(p => p.Id).AsQueryable();
            if (currentUser.Role.Name == CustomRole.Admin)
            {
                entities = _context.Users
                    .Where(x => x.Role.Name == CustomRole.Admin &&
                x.Id == currentUser.Id ||
                x.Role.Name == CustomRole.Employee)
                    .Include(r => r.Role)
                    .Include(t => t.Team)
                    .Where(x => !x.IsDeleted);
            }
            entities = (request.ProjectIds != null && request.ProjectIds
                .Any()) ? entities
                    .Where(p => p.ProjectUsers.Any(pu => request.ProjectIds
                    .Contains(pu.ProjectId))) : entities;

            entities = (request.TeamIds != null && request.TeamIds.Any()) ? entities
                .Where(p => p.Team != null && request.TeamIds.Contains(p.Team.Id)) : entities;

            var count = (await entities.ToListAsync()).Count;

            if (dto.SortField != null)
            {
                entities = dto.OrderBy
                    ? entities.OrderBy($"p=>p.{dto.SortField}")
                    : entities.OrderBy($"p=>p.{dto.SortField} descending");
            }

            if (dto.Skip != null && dto.Take != null)
            {
                entities = entities.Skip(dto.Skip.Value).Take(dto.Take.Value);
            }

            var userModel = _mapper.Map<List<UserDto>>(entities);
            var outputModel = new AllUserDto
            {
                Users = userModel,
                TotalCount = count
            };
            return outputModel;   
        }

        public async Task<UserGetByIdDto> GetUserById(int id)
        {
            AppUser entity = await _context.Users.Include(p => p.Role)
                .Include(m => m.Team).Include(m => m.ProjectUsers)
                .ThenInclude(m => m.Project).FirstOrDefaultAsync(x => x.Id == id);
            return entity == null ? throw new DomainException("Cannot found user!")
                : _mapper.Map<UserGetByIdDto>(entity);
        }
    }
}
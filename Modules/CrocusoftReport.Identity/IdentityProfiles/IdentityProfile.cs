﻿using AutoMapper;
using CrocusoftReport.Domain.AggregatesModel.AppUserAggregate;
using CrocusoftReport.Domain.AggregatesModel.ProjectAggregate;
using CrocusoftReport.Domain.AggregatesModel.RoleAggregate;
using CrocusoftReport.Identity.ViewModels;

namespace CrocusoftReport.Identity.IdentityProfiles
{
    public class IdentityProfile : Profile
    {
        public IdentityProfile()
        {
            CreateMap<Role, RoleDto>();
            CreateMap<AppUser, AllUserDto>();
            CreateMap<AppUser, UserInfoDto>()
                .ForMember(dest => dest.FullName, opt => opt
                .MapFrom(src => src.FirstName + " " + src.LastName));
            CreateMap<AppUser, UserDto>();
            CreateMap<AppUser, UserProfileDto>();
            CreateMap<AppUser, UserGetByIdDto>()
                .ForMember(dest => dest.Projects, opt => opt.
                MapFrom(src => src.ProjectUsers
                .Select(up => up.Project)));
            CreateMap<Project, UserProjectDto>();
        }
    }
}
﻿namespace CrocusoftReport.Identity.ViewModels
{
    public class AllUserDto
    {
        public IEnumerable<UserDto> Users { get; set; }
        public int TotalCount { get; set; }
    }
}
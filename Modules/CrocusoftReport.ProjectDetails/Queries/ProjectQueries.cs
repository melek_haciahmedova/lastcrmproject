﻿using AutoMapper;
using CrocusoftReport.Infrastructure.Constants;
using CrocusoftReport.Infrastructure.Database;
using CrocusoftReport.ProjectDetails.ViewModels;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Linq.Dynamic.Core;

namespace CrocusoftReport.ProjectDetails.Queries
{
    public class ProjectQueries : IProjectQueries
    {
        private readonly CrocusoftReportDbContext _context;
        private readonly IMapper _mapper;
        public ProjectQueries(CrocusoftReportDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<ProjectGetByIdDto> GetByIdAsync(int projectId)
        {
            var entity = await _context.Projects
            .Include(u => u.ProjectUsers)
            .ThenInclude(u => u.User)
            .FirstOrDefaultAsync(t => t.Id == projectId);
            return _mapper.Map<ProjectGetByIdDto>(entity);
        }

        public async Task<AllProjectDto> GetAllAsync(ProjectRequestDto request, LoadMoreDto dto)
        {
            var entities = _context.Projects.Where(p =>
            request.ProjectName == null || p.ProjectName.ToLower().Trim()
            .Contains(request.ProjectName.ToLower().Trim()))
              .OrderByDescending(p => p.Id).AsQueryable();

            var count = (await entities.ToListAsync()).Count;

            if (dto.SortField != null)
            {
                entities = dto.OrderBy
                    ? entities.OrderBy($"p=>p.{dto.SortField}")
                    : entities.OrderBy($"p=>p.{dto.SortField} descending");
            }

            if (dto.Skip != null && dto.Take != null)
            {
                entities = entities.Skip(dto.Skip.Value).Take(dto.Take.Value);
            }

            var projectModel = _mapper.Map<List<ProjectDto>>(entities);
            var outputModel = new AllProjectDto
            {
                Projects = projectModel,
                TotalCount = count
            };
            return outputModel;
        }
    }
}
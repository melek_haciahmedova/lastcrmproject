﻿namespace CrocusoftReport.ProjectDetails.ViewModels
{
    public class ProjectRequestDto
    {
        public string? ProjectName { get; set; }
    }
}
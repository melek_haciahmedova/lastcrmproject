﻿namespace CrocusoftReport.ProjectDetails.ViewModels
{
    public class ProjectDto
    {
        public int Id { get; set; }
        public string ProjectName { get; set; }
    }
}
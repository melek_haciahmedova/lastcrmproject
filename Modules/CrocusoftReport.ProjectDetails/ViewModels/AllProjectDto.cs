﻿namespace CrocusoftReport.ProjectDetails.ViewModels
{
    public class AllProjectDto
    {
        public IEnumerable<ProjectDto> Projects { get; set; }
        public int TotalCount { get; set; }
    }
}
﻿using MediatR;

namespace CrocusoftReport.ProjectDetails.Commands
{
    public class UpdateProjectCommand : IRequest<bool>
    {
        public int Id { get; set; }
        public string ProjectName { get; set; }
        public List<int>? UserIds { get; set; }
    }
}
﻿using FluentValidation;

namespace CrocusoftReport.ProjectDetails.Commands
{
    public class CreateProjectCommandValidator : AbstractValidator<CreateProjectCommand>
    {
        public CreateProjectCommandValidator() : base()
        {
            RuleFor(p => p.ProjectName).NotNull().NotEmpty().MaximumLength(50);
        }
    }
}
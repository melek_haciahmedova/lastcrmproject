﻿using CrocusoftReport.SharedKernel.Domain.Seedwork;

namespace CrocusoftReport.Domain.AggregatesModel.ProjectAggregate
{
    public interface IProjectRepository : IRepository<Project>
    {
    }
}
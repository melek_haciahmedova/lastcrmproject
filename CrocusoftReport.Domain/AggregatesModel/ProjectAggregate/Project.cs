﻿using CrocusoftReport.Domain.AggregatesModel.AppUserAggregate;
using CrocusoftReport.Domain.AggregatesModel.ProjectUserAggregate;
using CrocusoftReport.Domain.AggregatesModel.ReportAggregate;
using CrocusoftReport.SharedKernel.Domain.Seedwork;

namespace CrocusoftReport.Domain.AggregatesModel.ProjectAggregate
{
    public class Project : BaseEntity, IAggregateRoot
    {
        public string ProjectName { get; private set; }
        public List<ProjectUser>? ProjectUsers { get; private set; }
        public List<Report> Reports { get; private set; }
        public Project()
        {
            ProjectUsers = new List<ProjectUser>();
            Reports = new List<Report>();
        }
        public void SetDetails(string projectName)
        {
            ProjectName = projectName;
        }
        public void AddUsers(List<AppUser> users)
        {
            ProjectUsers ??= new List<ProjectUser>();

            var projectUsersToAdd = users.Select(user =>
            {
                var projectUser = new ProjectUser();
                projectUser.SetAppUser(user.Id);
                return projectUser;
            });
            ProjectUsers.AddRange(projectUsersToAdd);
        }
    }
}
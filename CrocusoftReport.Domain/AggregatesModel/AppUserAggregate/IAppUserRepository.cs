﻿using CrocusoftReport.Domain.AggregatesModel.AppUserAggregate;
using CrocusoftReport.SharedKernel.Domain.Seedwork;

namespace CrocusoftReport.Domain.AggregatesModel.UserAggregate
{
    public interface IAppUserRepository : IRepository<AppUser>
    {
    }
}
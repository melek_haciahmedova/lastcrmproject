﻿namespace CrocusoftReport.Domain.AggregatesModel.RoleAggregate
{
    public class RoleName
    {
        public const string SuperAdmin = nameof(SuperAdmin);
        public const string Admin = nameof(Admin);
        public const string Head = nameof(Head);
        public const string Employee = nameof(Employee);
    }
}
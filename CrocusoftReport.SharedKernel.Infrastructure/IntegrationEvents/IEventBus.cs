﻿namespace CrocusoftReport.SharedKernel.Infrastructure.IntegrationEvents
{
    public interface IEventBus
    {
        Task PublishAsync(string topicName, IntegrationEvent integrationEvent);
    }
}
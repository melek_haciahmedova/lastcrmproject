﻿using CrocusoftReport.SharedKernel.Domain.Seedwork;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace CrocusoftReport.SharedKernel.Infrastructure
{
    public abstract class Repository<T> : IRepository<T> where T : class, IAggregateRoot
    {
        public abstract DbContext Context { get; protected set; }
        public IUnitOfWork UnitOfWork => Context as IUnitOfWork;
        public async Task<T> AddAsync(T entity)
        {
            var entry = await Context.Set<T>().AddAsync(entity);
            return entry.Entity;
        }
        public async Task<T> GetByIdAsync(int id)
        {
            return await Context.Set<T>().FindAsync(id);
        }
        public T Update(T entity)
        {
            Context.Entry(entity).State = EntityState.Modified;
            return entity;
        }
        public bool Delete(T entity)
        {
            Context.Set<T>().Remove(entity);
            return true;
        }
        public Task<List<T>> GetAllAsync()
        {
            return Context.Set<T>().ToListAsync();
        }
        public T Get(Expression<Func<T, bool>> exp, params string[] includes)
        {
            var query = Context.Set<T>().AsQueryable();

            if (includes != null)
            {
                foreach (var reff in includes)
                {
                    query = query.Include(reff);
                }
            }
            return query.FirstOrDefault(exp);
        }
    }
}
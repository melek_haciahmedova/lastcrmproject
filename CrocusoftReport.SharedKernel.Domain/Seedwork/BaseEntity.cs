﻿using MediatR;
using System.ComponentModel.DataAnnotations.Schema;

namespace CrocusoftReport.SharedKernel.Domain.Seedwork
{
    public abstract class BaseEntity
    {
        public int Id { get; set; }
        [NotMapped]
        public List<INotification> DomainEvents { get; private set; }
        public void AddDomainEvent(INotification eventItem)
        {
            DomainEvents = DomainEvents ?? new List<INotification>();
            DomainEvents.Add(eventItem);
        }
        public void RemoveDomainEvent(INotification eventItem)
        {
            DomainEvents?.Remove(eventItem);
        }
        public bool IsTransient()
        {
            return Id == default;
        }
        public override bool Equals(object obj)
        {
            if (obj is not BaseEntity)
                return false;

            if (ReferenceEquals(this, obj))
                return true;

            if (GetType() != obj.GetType())
                return false;

            var item = (BaseEntity)obj;

            if (item.IsTransient() || IsTransient())
                return false;
            return item.Id == Id;
        }

        private int? _requestedHashCode;
        public override int GetHashCode()
        {
            if (IsTransient()) return base.GetHashCode();

            if (!_requestedHashCode.HasValue)
                _requestedHashCode =
                    Id.GetHashCode() ^
                    31; // XOR for random distribution (http://blogs.msdn.com/b/ericlippert/archive/2011/02/28/guidelines-and-rules-for-gethashcode.aspx)
            return _requestedHashCode.Value;
        }
        public static bool operator ==(BaseEntity left, BaseEntity right)
        {
            return left?.Equals(right) ?? Equals(right, null);
        }
        public static bool operator !=(BaseEntity left, BaseEntity right)
        {
            return !(left == right);
        }
    }
}